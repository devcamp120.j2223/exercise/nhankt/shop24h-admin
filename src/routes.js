import React from 'react'

const Product = React.lazy(() => import("./components/product/Product"))
const Customer = React.lazy(() => import("./components/customer/Customer"))
const ProductType = React.lazy(() => import("./components/productType/ProductType"))
const Order = React.lazy(() => import("./components/order/Order"))
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/products', name: 'Product', element: Product },
  { path: '/customers', name: 'Customer', element: Customer },
  { path: '/productTypes', name: 'ProductType', element: ProductType },
  { path: '/orders', name: 'Order', element: Order },
  
]

export default routes
