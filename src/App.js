import React, { Component, Suspense } from 'react'
import '@coreui/coreui/dist/css/coreui.min.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import AppSidebar from './components/AppSidebar';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Product from './components/product/Product';
import './scss/style.scss'
const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)
const DefaultLayout = React.lazy(() => import('./layout/DefaultLayout'))

function App() {
  return (
    <BrowserRouter>
      
    <Suspense fallback={loading}>
      <Routes>
        <Route path="*" name="Home" element={<DefaultLayout />} />
      </Routes>
    </Suspense>
  </BrowserRouter>
  );
}

export default App;
