
import { Modal, Box, Typography, Select, MenuItem, Button, Snackbar, Alert,Grid,TextField } from "@mui/material";
import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";

import { Col, Row, Label, Input } from "reactstrap"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


function AddModal({ isOpen, handleCloseModal, addCustomer,types }) {
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }

    var vNewCustomer = {
        fullName: "",
        phone: "",
        email: "",
        orders: [],
        type:""
    }
    const [display, setDisplay] = useState(isOpen)
    const [newCustomer, setNewCustomer] = useState(vNewCustomer)
    const [detailTypes, setDetailTypes] = useState(types)
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    useEffect(() => {
        setDisplay(isOpen)
        setDetailTypes(types)
    }, [isOpen])
    
    
    const checkValidOrder = (paramOrder) => {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (paramOrder==""){
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("test")
            return false
        }
       
        return true
    }
    const handleConfirmBtn = (e) => {
        let isValid = checkValidOrder(newCustomer)
        if (isValid) {
           e.preventDefault();
           addCustomer(newCustomer)
        }

    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    useEffect(() => {
        setNewCustomer(vNewCustomer)
        setDisplay(isOpen)
    }, [isOpen])
    const handleChangeFullName = (event) => {
        setNewCustomer({
            ...newCustomer,
            fullName: event.target.value
        })
    }
    const handleChangePhone = (event) => {
        setNewCustomer({
            ...newCustomer,
            phone: event.target.value
        })
    }
    const handleChangeEmail = (event) => {
        setNewCustomer({
            ...newCustomer,
            email: event.target.value
        })
    }
    
    
    return (
        <div>
            <Modal
                open={display}
                onClose={() => { setDisplay(!display); handleCloseModal() }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box onSubmit={handleConfirmBtn} sx={style} component="form">
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Thêm khách hàng mới
                    </Typography>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={12}>
                            <Grid><Label>Họ tên khách hàng</Label></Grid>
                            <Grid ><TextField required onChange={handleChangeFullName} size="small" value={newCustomer.fullName} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        </Grid>
                        <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Số điện thoại</Label></Grid>
                            <Grid ><TextField required size="small" onChange={handleChangePhone} value={newCustomer.phone} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Email</Label></Grid>
                            <Grid ><TextField required onChange={handleChangeEmail} size="small" value={newCustomer.email} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                    </Grid>
                    
                    

                    <Grid container>
                        <Grid item xs={4} mx="auto" >
                            <Button variant="contained" color="info" type="submit" >Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { handleCloseModal() }}>Cancle</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
export default AddModal