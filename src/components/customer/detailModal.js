
import { Modal, Box, Typography, Select, MenuItem, Button, CardMedia, TextField, Grid, } from "@mui/material";
import { useEffect, useState } from "react";
import { Label } from "reactstrap"

import NumberFormat from "react-number-format";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 750,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 3.5,
};


function DetailModal({ isOpen, handleCloseModal, data, updateCustomer }) {
    const citysArray = require(`../../assets/locations/cities.json`).data

    const [display, setDisplay] = useState(isOpen)
    const [detailData, setDetailData] = useState(data)
    const [selectedCity, setSelectedCity] = useState("")
    const [selectedDistrict, setSelectedDistrict] = useState("")
    const [selectedWard, setSelectedWard] = useState("")
    const [street, setStreet] = useState("")
    const [districtsArray, setDistrictsArray] = useState([])
    const [wardsArray, setWardsArray] = useState([])
    const [initCustomerAddress, setInitCustomerAddress] = useState({ city: "", district: "", ward: "", street: "", state: 0 })

    const [refreshPage, setRefreshPage] = useState(Boolean(true))
    const customer = {
        _id: detailData._id,
        fullName: detailData.fullName,
        phone: detailData.phone,
        email: detailData.email,
        address: {
            city: citysArray.filter(el => el.id == selectedCity)[0] ? citysArray.filter(el => el.id == selectedCity)[0].name : "",
            district: districtsArray.filter(el => el.id == selectedDistrict)[0] ? districtsArray.filter(el => el.id == selectedDistrict)[0].name : "",
            ward: wardsArray.filter(el => el.id == selectedWard)[0] ? wardsArray.filter(el => el.id == selectedWard)[0].name : "",
            street: street
        },
    }
    useEffect(() => {
        setDisplay(isOpen)
        setDetailData(data)
    }, [isOpen,data])
    useEffect(()=>{
        console.log(detailData)
        setSelectedCity(detailData.address && citysArray.length>0 ?citysArray.filter(el => el.name == detailData.address.city)[0].id:"")
        setStreet(detailData.length>0?data.address.street:"")
        setInitCustomerAddress({ ...initCustomerAddress, ...detailData.address })
    },[detailData])
    useEffect(() => {
        if (selectedDistrict == "" && districtsArray.length > 0 && initCustomerAddress.district != "" && initCustomerAddress.state < 2) {

            console.log(initCustomerAddress)
            setSelectedDistrict(districtsArray.filter(el => el.name == initCustomerAddress.district)[0].id)
            setInitCustomerAddress({ ...initCustomerAddress, state: initCustomerAddress.state + 1 })

        }
    }, [districtsArray, initCustomerAddress])
    useEffect(() => {
        if (selectedWard == "" && wardsArray.length > 0 && initCustomerAddress.ward != "" && initCustomerAddress.state < 2) {
            setSelectedWard(wardsArray.filter(el => el.name == initCustomerAddress.ward)[0].id)
            setInitCustomerAddress({ ...initCustomerAddress, state: initCustomerAddress.state + 1 })

        }

    }, [wardsArray, initCustomerAddress])
    useEffect(() => {
        console.log(selectedCity)
        if (selectedCity != "") {

            let newDistrictArray = require(`../../assets/locations/districts/${selectedCity}.json`).data
            setDistrictsArray(newDistrictArray)
            setWardsArray([])
            //console.log(newDistrictArray)
        }
    }, [selectedCity])
    useEffect(() => {
        if (selectedDistrict != "") {

            let newWardArray = require(`../../assets/locations/wards/${selectedDistrict}.json`).data
            setWardsArray(newWardArray)
            //console.log(newWardArray)
        }
    }, [selectedDistrict])

    const handleChangeFullName = (event) => {
        setDetailData({
            ...detailData,
            fullName: event.target.value
        })
    }
    const handleChangePhone = (event) => {
        setDetailData({
            ...detailData,
            phone: event.target.value
        })
    }
    const handleChangeEmail = (event) => {
        setDetailData({
            ...detailData,
            email: event.target.value
        })
    }
    const handleChangeSelectedCity = (event) => {
        setSelectedCity(event.target.value)

    }
    const handleChangeSelectedDistrict = (event) => {
        setSelectedDistrict(event.target.value)
        setSelectedWard("")
    }
    const handleChangeSelectedWard = (event) => {
        setSelectedWard(event.target.value)
    }
    const handleStreet = (event) => {
        setStreet(event.target.value)
    }

    return (
        <div>
            <Modal
                open={display}
                onClose={() => { setDisplay(!display); handleCloseModal() }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                width="400px"
            >
                <Box onSubmit={(e) => { e.preventDefault(); updateCustomer(customer) }} sx={style} component="form">
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Chi tiết Khách hàng
                    </Typography>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Id</Label></Grid>
                            <Grid ><TextField size="small" disabled value={detailData._id} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Họ tên khách hàng</Label></Grid>
                            <Grid ><TextField onChange={handleChangeFullName} size="small" value={detailData.fullName} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                    </Grid>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Số điện thoại</Label></Grid>
                            <Grid ><TextField size="small" onChange={handleChangePhone} value={detailData.phone} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Email</Label></Grid>
                            <Grid ><TextField onChange={handleChangeEmail} size="small" value={detailData.email} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginLeft: "5px" }} >
                        <Grid item xs={12} md={6} mb={4} mx="auto">
                            <TextField
                                style={{ width: "90%" }}
                                size="small"
                                label="Tỉnh/Thành Phố"
                                select
                                value={selectedCity}
                                onChange={handleChangeSelectedCity}
                            >
                                {citysArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                            </TextField>
                        </Grid>
                        <Grid item xs={12} md={6} mb={4} mx="auto">
                            <TextField
                                select
                                style={{ width: "90%" }}
                                size="small"
                                label="Quận/Huyện"
                                value={selectedDistrict}
                                onChange={handleChangeSelectedDistrict}
                            >
                                {districtsArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginLeft: "5px" }} >
                        <Grid item xs={12} md={6} mb={4} mx="auto">
                            <TextField
                                select
                                style={{ width: "90%" }}
                                size="small"
                                label="Phường/Xã"
                                value={selectedWard}
                                onChange={handleChangeSelectedWard}
                            >
                                {wardsArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                            </TextField>
                        </Grid>
                        <Grid item xs={12} md={6} mb={4} mx="auto" >
                            <TextField
                                style={{ width: "90%" }}
                                size="small"
                                label="Đường, số nhà"
                                value={street}
                                onChange={handleStreet}
                            />
                        </Grid>
                    </Grid>

                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Ngày khởi tạo</Label></Grid>
                            <Grid ><TextField style={{ width: "90%" }} type="date" size="small" disabled
                                value={Object.keys(detailData).length > 0 ? `${detailData.timeCreated.slice(0, 4)}-${detailData.timeCreated.slice(5, 7)}-${detailData.timeCreated.slice(8, 10)}` : ""}
                            ></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Ngày cập nhật</Label></Grid>
                            <Grid ><TextField size="small" type="date" style={{ width: "90%" }} disabled
                                value={Object.keys(detailData).length > 0 ? `${detailData.timeUpdated.slice(0, 4)}-${detailData.timeUpdated.slice(5, 7)}-${detailData.timeUpdated.slice(8, 10)}` : ""}
                            ></TextField></Grid>
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={4} mx="auto" >
                            <Button variant="contained" color="info" type="submit" >Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { handleCloseModal() }}>Cancle</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </div>
    );
}
export default DetailModal