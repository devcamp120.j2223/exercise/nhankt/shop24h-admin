
import { Modal, Box, Typography, Select, MenuItem, Button, CardMedia, TextField, Grid, Snackbar, Alert } from "@mui/material";
import { useEffect, useState } from "react";
import { Label } from "reactstrap"

import NumberFormat from "react-number-format";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 750,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 3.5,
    maxHeight:'90%',
    overflow:'scroll',
};


function OrderModal({ isOpen, handleCloseModal, data, updateOrder }) {
    const [display, setDisplay] = useState(isOpen)
    const [allProducts, setAllProducts] = useState([])

    //orders là mảng các order đươc fetch về từ mảng orderId truyền qua data của customer đang được chọn
    const [orders, setOrders] = useState([])

    //indexOfOrder index của order được chọn từ mảng order để hiển thị ra modal
    const [indexOfOrder, setIndexOfOrder] = useState(0)
    const [refreshPage, setRefreshPage] = useState(Boolean(true))

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")

    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    useEffect(() => {
        if (Object.keys(data).length > 0) {
            if (data.orders.length > 0) {
                getData("http://localhost:8000/orders?id=" + data.orders.join())
                    .then(res => setOrders(res.data))
                    .then(console.log(orders.length))
            }
            else {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("không có đơn hàng")
                setDisplay(false);
                handleCloseModal()
            }

        }
        setDisplay(isOpen)
    }, [isOpen])
    useEffect(() => {
        getData("http://localhost:8000/product")
            .then(res => setAllProducts(res.data))
    }, [])

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const handleChangeOrder = (event) => {
        setIndexOfOrder(event.target.value)
    }
    const handleChangeCost = (event) => {
        const { formattedValue, value } = event;
        setOrders(
            orders.map((el, index) => index == indexOfOrder ? { ...el, cost: value } : el)
        )
    }
    const handleChangeOrderStatus = (event) => {
        setOrders(
            orders.map((el, index) => index == indexOfOrder ? { ...el, orderStatus: event.target.value } : el)
        )
    }
    const handleChangeName = (event, index1) => {
        setOrders(
            orders.map((el, index) => index == indexOfOrder ? { ...el, orderDetail: el.orderDetail.map((e, i) => i == index1 ? { ...e, name: event.target.value } : e) } : el)
        )
    }
    const handleChangeQuantity = (event, index1) => {
        setOrders(
            orders.map((el, index) => index == indexOfOrder ? { ...el, orderDetail: el.orderDetail.map((e, i) => i == index1 ? { ...e, quantity: event.target.value } : e) } : el)
        )
    }
    const handleChangeCity = (event) => {
        setOrders(
            orders.map((el, index) => index == indexOfOrder ? { ...el, shippingAddress: { ...el.shippingAddress, city: event.target.value } } : el)
        )
    }
    const handleChangeDistrict = (event) => {
        setOrders(
            orders.map((el, index) => index == indexOfOrder ? { ...el, shippingAddress: { ...el.shippingAddress, district: event.target.value } } : el)
        )
    }
    const handleChangeWard = (event) => {
        setOrders(
            orders.map((el, index) => index == indexOfOrder ? { ...el, shippingAddress: { ...el.shippingAddress, ward: event.target.value } } : el)
        )
    }
    const handleChangeStreet = (event) => {
        setOrders(
            orders.map((el, index) => index == indexOfOrder ? { ...el, shippingAddress: { ...el.shippingAddress, street: event.target.value } } : el)
        )
    }

    return (
        <div>
            <Modal
                open={display}
                onClose={() => { setDisplay(!display); handleCloseModal() }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                width="400px"
            >
                <Box onSubmit={(e) => { e.preventDefault(); updateOrder(orders[indexOfOrder]) }} sx={style} component="form">
                    <Grid container mb={2} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Id</Label></Grid>
                            <Grid >
                                <TextField size="small"
                                    select
                                    value={indexOfOrder}
                                    style={{ width: "90%" }}
                                    onChange={handleChangeOrder}
                                >
                                    {orders.map((el, index) => <MenuItem key={index} value={index}>{el._id}</MenuItem>)}
                                </TextField>
                            </Grid>
                        </Grid>
                        <Grid item xs={6}  >
                            <Grid><Label>Ngày tạo đơn</Label></Grid>
                            <Grid >
                                <TextField size="small"
                                    select
                                    value={indexOfOrder}
                                    style={{ width: "90%" }}
                                    onChange={handleChangeOrder}
                                >
                                    {orders.map((el, index) => <MenuItem key={index} value={index}>{(el.timeCreated)}</MenuItem>)}
                                </TextField>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={6} mx="auto" borderBottom={0.5} my={1}></Grid>
                    </Grid>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Chi tiết Order
                    </Typography>
                    <Grid container mb={2} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}>
                            <Grid><Label>Tình trạng đơn hàng</Label></Grid>
                            <Grid ><TextField size="small"
                                select
                                value={orders.length > 0 ? orders[indexOfOrder].orderStatus : ""}
                                style={{ width: "90%" }}
                                onChange={handleChangeOrderStatus}
                            >
                                <MenuItem value="cancle">Đã huỷ</MenuItem>
                                <MenuItem value="open">Chờ xác nhận</MenuItem>
                                <MenuItem value="confirm">Đã xác nhận</MenuItem>
                                <MenuItem value="shipping">Đang giao hàng</MenuItem>
                                <MenuItem value="done">Đã hoàn thành</MenuItem>
                            </TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Giá tiền</Label></Grid>
                            <Grid><NumberFormat disabled required onValueChange={handleChangeCost} thousandSeparator={true} value={orders.length > 0 ? orders[indexOfOrder].cost : ""} style={{ width: "90%", height: "2.3rem" }}></NumberFormat></Grid>
                        </Grid>
                    </Grid>
                    <Grid container mb={1} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}>
                            <Grid><Label>Tên sản phẩm</Label></Grid>
                        </Grid>
                        <Grid item xs={2}>
                            <Grid><Label>Số lượng</Label></Grid>
                        </Grid>
                        <Grid item xs={4}>
                            <Grid textAlign="center"><Label>Hình ảnh</Label></Grid>
                        </Grid>
                    </Grid>
                    {orders.length > 0 ? orders[indexOfOrder].orderDetail.map((el, index) => {
                        return (<Grid key={index} container mb={2} style={{ marginLeft: "5px" }}>
                            <Grid item xs={6}>
                                <Grid><TextField disabled required size="small" value={el.name} onChange={(e) => handleChangeName(e, index)} style={{ width: "90%" }}></TextField></Grid>
                            </Grid>
                            
                            <Grid item xs={2}>
                                <Grid><TextField disabled required size="small" value={el.quantity} onChange={(e) => handleChangeQuantity(e, index)} style={{ width: "90%" }}></TextField></Grid>
                            </Grid>
                            <Grid item xs={4}>
                            <img style={{marginLeft:"30%"}} src={allProducts.filter(e=>e._id==el.productId)[0].imageUrl} height="80px"></img>
                                
                            </Grid>
                        </Grid>)
                    }) : ""}
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Địa chỉ giao hàng
                    </Typography>
                    <Grid container mb={2} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}>
                            <Grid><Label>Thành Phố</Label></Grid>
                            <Grid><TextField disabled required size="small" value={orders.length > 0 ? orders[indexOfOrder].shippingAddress.city : ""} onChange={handleChangeCity} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Quận/huyện</Label></Grid>
                            <Grid><TextField disabled required size="small" value={orders.length > 0 ? orders[indexOfOrder].shippingAddress.district : ""} onChange={handleChangeDistrict} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                    </Grid>
                    <Grid container mb={2} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}>
                            <Grid><Label>Phường</Label></Grid>
                            <Grid><TextField disabled required size="small" value={orders.length > 0 ? orders[indexOfOrder].shippingAddress.ward : ""} onChange={handleChangeWard} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Đường, số nhà</Label></Grid>
                            <Grid><TextField disabled required size="small" value={orders.length > 0 ? orders[indexOfOrder].shippingAddress.street : ""} onChange={handleChangeStreet} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={4} mx="auto" >
                            <Button variant="contained" color="info" type="submit" >Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { handleCloseModal() }}>Cancle</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
export default OrderModal