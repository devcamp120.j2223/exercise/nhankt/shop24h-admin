import { Button, TextField, MenuItem, Typography, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Pagination, Snackbar, Alert } from "@mui/material"
import { useEffect, useState } from "react";
import AppSidebar from "../AppSidebar";
import Header from "../Header";
import AddModal from "./addModal";
import DeleteModal from "./deleteModal";
import DetailModal from "./detailModal";
import OrderModal from "./orderModal"





export default function Customer() {
    const [rowsPerPage, setRowsPerPage] = useState(8);
    const [customers, setCustomers] = useState([])
    const [showCustomers, setShowCustomers] = useState([])
    const [noPage, setNoPage] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [refreshPage, setRefreshPage] = useState(Boolean(true))
    const [filter, setFilter] = useState({ field: 0, data: "" })
    const [filterData,setFilterData] = useState([])
    //state hiển thị các modal
    const [DetailModalDisplay, setDetailModalDisPlay] = useState(Boolean(false))
    const [DeleteModalDisplay, setDeleteModalDisPlay] = useState(Boolean(false))
    const [AddModalDisplay, setAddModalDisPlay] = useState(Boolean(false))
    const [OrderModalDisplay, setOrderModalDisplay] = useState(Boolean(false))

    
    //state dữ liệu khi ấn các nút của 1 order
    const [dataOfRow, setDataOfRow] = useState({})
    //state thông báo
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    //fetch
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    //sự kiện load trang, tải dữ liệu
    useEffect(() => {
        getData("http://localhost:8000/customer")
            .then((data) => {
                setCustomers(data.data)
                setFilterData(data.data)
            })
    }, [refreshPage])
    useEffect(() => {
        if (filter.field=="id"){
            setFilterData(customers.filter(el=>(el._id).toLowerCase().includes(filter.data)))
        }
        else if (filter.field=="name"){
            setFilterData(customers.filter(el=>(el.fullName).toLowerCase().includes(filter.data)))
        }
        else if (filter.field=="phone"){
            setFilterData(customers.filter(el=>(el.phone).toLowerCase().includes(filter.data)))
        }
        else if (filter.field=="email"){
            setFilterData(customers.filter(el=>(el.email).toLowerCase().includes(filter.data)))
        }
        else {setFilterData(customers)}
    },[filter])
    useEffect(()=>{
        setFilter({ ...filter, data:"" })
    },[filter.field])
    useEffect(() => {
       
                setNoPage(Math.ceil(filterData.length / rowsPerPage))
                setShowCustomers(filterData.slice((currentPage - 1) * rowsPerPage, currentPage * rowsPerPage));

    }, [currentPage, rowsPerPage, refreshPage,customers,filterData])//

    const changePage = (event, value) => {
        setCurrentPage(value)
    }
    const changeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value))
    }
    //bấm các nút để hiện modal
    const onDetailBtnClick = (paramCustomer) => {
        setDetailModalDisPlay(!DetailModalDisplay)
        setDataOfRow(paramCustomer)
    }
    const onDeleteBtnClick = (paramCustomer) => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
        setDataOfRow(paramCustomer)
    }
    const onOrderDetailBtnClick = (paramCustomer) => {
        setOrderModalDisplay(!OrderModalDisplay)
        setDataOfRow(paramCustomer)
    }
    
    const onBtnAddClick = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }

    //Xử lý khi đóng các modal components
    const closeDetailModal = () => {
        setDetailModalDisPlay(!DetailModalDisplay)
    }
    const closeDeleteModal = () => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
    }
    const closeAddModal = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }
    const closeOrderModal = () => {
        setOrderModalDisplay(false)
    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    //Xử lý update customer
    const handleUpdateCustomer = (paramCustomer) => {
        console.log(paramCustomer)
        let body = {
            method: 'PUT',
            body: JSON.stringify(
                paramCustomer
            ),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
        };

        getData("http://localhost:8000/customer/" + paramCustomer._id, body)
            .then((data) => {
                closeDetailModal();
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã sửa order")
                setRefreshPage(!refreshPage)
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("sửa thất bại!")
            })


    }
    //Xử lý delete Customer
    const handleDeleteCustomer = (paramCustomer) => {
        let body = {
            method: 'DELETE',
        };
        getData("http://localhost:8000/customer/"+ paramCustomer._id,body)
        .then((data) => {
            setOpenAlert(true);
            setStatusModal("success")
            setNoidungAlertValid("đã xoá nhóm sản phẩm")
            closeDeleteModal();
            setRefreshPage(!refreshPage)
        })
        .catch((error) => {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Dữ liệu xoá thất bại!")
        })
    }

    //Xử lý add Order
    const handleAddCustomer = (paramCustomer) => {
        const body = {
            method: 'POST',
            body: JSON.stringify(paramCustomer),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("http://localhost:8000/customer/", body)
            .then((data) => {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã thêm order")
                closeAddModal();
                setRefreshPage(!refreshPage)
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu thêm thất bại!")
            })

    }
    //Xử lý sửa Order
    const handleUpdateOrder = (paramOrder) => {
        console.log(paramOrder)
        let body = {
            method: 'PUT',
            body: JSON.stringify(paramOrder),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("http://localhost:8000/orders/"+paramOrder._id, body)
            .then((data) => {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã sữa order")
                closeOrderModal();
                setRefreshPage(!refreshPage)
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu thêm thất bại!")
            })

    }
    

    return (
        <div >
            <Grid container>
                <Grid item xs={12}>
                    <>
                        <Grid container className="mb-3">
                            <Grid item xs={2}>
                                <label >Show
                                    <input style={{ width: "5rem", textAlign: "center" }} type={"number"} value={rowsPerPage} onChange={changeRowsPerPage}>
                                    </input> entries
                                </label>
                            </Grid>

                            <Grid item xs={8} justifyItems="center" >
                                <Grid container direction="row" justifyContent="center" >
                                    <Typography style={{ width: "7%", marginRight: "2rem", marginTop:"4px" }}>Bộ lọc</Typography>
                                    <TextField
                                        label="tìm theo trường" select size="small" value={filter.field} style={{ width: "20%", marginRight: "2rem" }}
                                        onChange={(e) => setFilter({ ...filter, field: e.target.value })}>
                                        <MenuItem value={0} >Tắt bộ lọc </MenuItem>
                                        <MenuItem value="id">id</MenuItem>
                                        <MenuItem value="name">Tên</MenuItem>
                                        <MenuItem value="phone">Số điện thoại</MenuItem>
                                        <MenuItem value="email">Email</MenuItem>

                                    </TextField>
                                    <TextField placeholder="điền thông tin cần tìm" size="small" style={{ width: "25%" }}
                                    value={filter.data}
                                    onChange={(e) => setFilter({ ...filter, data: e.target.value })}>
                                    </TextField>
                                </Grid>
                            </Grid>


                            <Grid item xs={2} style={{ justifyContent: "flex-end" }}>
                                <Button variant="contained" color="success" onClick={onBtnAddClick} >Thêm khách hàng</Button>
                            </Grid>
                        </Grid>
                        <Grid>
                            <Grid item>
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                
                                                <TableCell align="right">Tên</TableCell>
                                                <TableCell align="right">Số điện thoại</TableCell>
                                                <TableCell align="right">email</TableCell>

                                                <TableCell align="right" >Đơn hàng</TableCell>
                                                <TableCell align="center">Chi tiết</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {showCustomers.map((customer, index) => (
                                                <TableRow
                                                    key={index}
                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                >
                                                    
                                                    <TableCell align="right">{customer.fullName}</TableCell>


                                                    <TableCell align="right">{customer.phone}</TableCell>
                                                    <TableCell align="right">{customer.email}</TableCell>

                                                    <TableCell align="right">
                                                        <Button variant="contained" color="info" onClick={() => onOrderDetailBtnClick(customer)}>Thông tin đơn hàng</Button>
                                                    </TableCell>
                                                    <TableCell align="center" >
                                                        <Button variant="contained" color="info" onClick={() => onDetailBtnClick(customer)}>Detail</Button>
                                                        <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => onDeleteBtnClick(customer)}>Delete</Button>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>
                        <Grid container mt={3} mb={2} justifyContent={"flex-end"}>
                            <Grid item>
                                <Pagination count={noPage} variant="outlined" onChange={changePage} />
                            </Grid>
                        </Grid>

                    </>
                </Grid>
            </Grid>


            
            
            <DetailModal isOpen={DetailModalDisplay} handleCloseModal={closeDetailModal} data={dataOfRow} updateCustomer={handleUpdateCustomer}></DetailModal>
            <OrderModal isOpen={OrderModalDisplay} handleCloseModal={closeOrderModal} data={dataOfRow} updateOrder={handleUpdateOrder}></OrderModal>
            <DeleteModal isOpen={DeleteModalDisplay} handleCloseModal={closeDeleteModal} data={dataOfRow} deleteCustomer={handleDeleteCustomer}></DeleteModal>
            <AddModal isOpen={AddModalDisplay} handleCloseModal={closeAddModal} addCustomer={handleAddCustomer}></AddModal> 
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
