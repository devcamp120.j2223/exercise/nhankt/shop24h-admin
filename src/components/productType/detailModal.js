
import { Modal, Box, Typography, Select, MenuItem, Button, CardMedia, TextField, Grid } from "@mui/material";
import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";
import { Label } from "reactstrap"


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 750,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 3.5,
};


function DetailModal({ isOpen, handleCloseModal, data, updateType }) {
    const [display, setDisplay] = useState(isOpen)
    const [detailData, setDetailData] = useState(data)

    const [refreshPage, setRefreshPage] = useState(Boolean(true))
    useEffect(() => {
        setDisplay(isOpen)
        setDetailData(data)
    }, [isOpen])
    

    const handleChangeName = (event) => {
        setDetailData({
            ...detailData,
            name: event.target.value
        })
    }
   
    const handleChangeDescription = (event) => {
        setDetailData({
            ...detailData,
            description: event.target.value
        })
    }
   
    
    return (
        <div>
            <Modal
                open={display}
                onClose={() => { setDisplay(!display); handleCloseModal() }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box onSubmit={(e)=>{ e.preventDefault();updateType(detailData)}} sx={style} component="form">
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Chi tiết loại sản phẩm
                    </Typography>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Id</Label></Grid>
                            <Grid ><TextField size="small" disabled value={detailData._id} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Tên sản phẩm</Label></Grid>
                            <Grid ><TextField onChange={handleChangeName} size="small" value={detailData.name} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                    </Grid>
                    
                    
                    <Grid container mb={2} style={{ marginLeft: "5px" }}>
                        
                        <Grid item xs={12}  >
                            <Grid><Label>Mô tả</Label></Grid>
                            <Grid><textarea onChange={handleChangeDescription} rows={8} value={detailData.description} style={{ width: "95%" }}></textarea></Grid>
                        
                        </Grid>
                    </Grid>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Ngày khởi tạo</Label></Grid>
                            <Grid ><TextField style={{width:"90%"}} type="date" size="small" disabled
                                value={Object.keys(detailData).length > 0 ? `${detailData.timeCreated.slice(0,4)}-${detailData.timeCreated.slice(5, 7)}-${detailData.timeCreated.slice(8,10)}` : ""}
                            ></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Ngày cập nhật</Label></Grid>
                            <Grid ><TextField size="small" type="date" style={{width:"90%"}} disabled
                                value={Object.keys(detailData).length > 0 ? `${detailData.timeUpdated.slice(0,4)}-${detailData.timeUpdated.slice(5, 7)}-${detailData.timeUpdated.slice(8,10)}` : ""}
                            ></TextField></Grid>
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={4} mx="auto" >
                            <Button variant="contained" color="info" type="submit" >Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { handleCloseModal() }}>Cancle</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </div>
    );
}
export default DetailModal