
import { Modal, Box, Typography, Select, MenuItem, Button, Snackbar, Alert, Grid, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";

import { Col, Row, Label, Input } from "reactstrap"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


function AddModal({ isOpen, handleCloseModal, addProductType }) {
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }

    var vNewType = {
        name: "",
        timeUpdated: ""
    }
    const [display, setDisplay] = useState(isOpen)
    const [newProductType, setNewProductType] = useState(vNewType)

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    useEffect(() => {
        setDisplay(isOpen)

    }, [isOpen])


    const checkValidOrder = (paramType) => {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (paramType==""){
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("test")
            return false
        }
       
        return true
    }
    const handleConfirmBtn = (e) => {
        let isValid = checkValidOrder(newProductType)
        if (isValid) {
           e.preventDefault();
           addProductType(newProductType)
        }

    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    useEffect(() => {
        setNewProductType(vNewType)
        setDisplay(isOpen)
    }, [isOpen])
    const handleChangeName = (event) => {
        setNewProductType({
            ...newProductType,
            name: event.target.value
        })
    }

    const handleChangeDescription = (event) => {
        setNewProductType({
            ...newProductType,
            description: event.target.value
        })
    }
   
    return (
        <div>
            <Modal
                open={display}
                onClose={() => { setDisplay(!display); handleCloseModal() }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box onSubmit={handleConfirmBtn} sx={style} component="form">
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Thêm loại sản phẩm mới
                    </Typography>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={12}>
                            <Grid><Label>Tên loại sản phẩm</Label></Grid>
                            <Grid ><TextField onChange={handleChangeName} size="small" value={newProductType.name} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>

                    </Grid>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={12}>
                            <Grid><Label>Mô tả</Label></Grid>
                            <Grid><textarea onChange={handleChangeDescription} rows={8} value={newProductType.description} style={{ width: "90%" }}></textarea></Grid>
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={4} mx="auto" >
                            <Button variant="contained" color="info" type="submit" >Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { handleCloseModal() }}>Cancle</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
export default AddModal