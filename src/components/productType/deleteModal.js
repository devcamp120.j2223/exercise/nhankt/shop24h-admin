
import { Modal, Box,Typography, Button } from "@mui/material";
import { useEffect, useState } from "react";
import { Col,Row} from "reactstrap"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  

function DeleteModal( {isOpen,handleCloseModal,data,deleteProductType}) {
    const [deleteData,setDeleteData]= useState(data)
    const [display,setDisplay]= useState(isOpen)
    useEffect(()=>{
        setDisplay(isOpen)
        setDeleteData(data)
    },[isOpen])
    return (
        <div>
            <Modal
                open={display}
                onClose= {()=>{setDisplay(!display) ;handleCloseModal()}}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {`Bạn chắc chắn muốn xoá loại sản phẩm ${deleteData.name} ???`}
                    </Typography>
                    <Row className="mt-4">
                        <Col sm="12" className="text-center" >
                            <Button variant="contained" color="info" onClick={()=>{deleteProductType(deleteData)}}>Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={()=>{handleCloseModal()}}>Cancle</Button>
                        </Col>
                    </Row>         
                </Box>    
            </Modal>
        </div>
    );
}
export default DeleteModal