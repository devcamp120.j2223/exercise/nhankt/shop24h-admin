import { Button, Container, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Pagination, Snackbar, CardMedia, Alert, TextField, MenuItem, Typography } from "@mui/material"
import { useEffect, useState } from "react";
import AppSidebar from "../AppSidebar";
import Header from "../Header";
import AddModal from "./addModal";
import DeleteModal from "./deleteModal";
import DetailModal from "./detailModal";





export default function ProductType() {
    const [rowsPerPage, setRowsPerPage] = useState(8);
    const [allTypes, setAllTypes] = useState([])
    const [showTypes, setShowTypes] = useState([])
    const [noPage, setNoPage] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [refreshPage, setRefreshPage] = useState(Boolean(true))
    const [filter, setFilter] = useState({ field: 0, data: "" })
    const [filterData,setFilterData] = useState([])
    //state hiển thị các modal
    const [DetailModalDisplay, setDetailModalDisPlay] = useState(Boolean(false))
    const [DeleteModalDisplay, setDeleteModalDisPlay] = useState(Boolean(false))
    const [AddModalDisplay, setAddModalDisPlay] = useState(Boolean(false))
    //state dữ liệu khi ấn các nút của 1 order
    const [dataOfRow, setDataOfRow] = useState({})
    //state thông báo
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    //fetch
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    //sự kiện load trang, tải dữ liệu
    useEffect(() => {
        getData("http://localhost:8000/productType")
            .then((data) => {
                setAllTypes(data.data)
                setFilterData(data.data)
            })
    }, [refreshPage])
    useEffect(()=>{
        setFilter({ ...filter, data:"" })
    },[filter.field])
    useEffect(() => {
        if (filter.field=="id"){
            setFilterData(allTypes.filter(el=>(el._id).toLowerCase().includes(filter.data)))
        }
        else if (filter.field=="name"){
            setFilterData(allTypes.filter(el=>(el.name).toLowerCase().includes(filter.data)))
        }
        else {setFilterData(allTypes)}
    },[filter])
    useEffect(() => {

        setNoPage(Math.ceil(filterData.length / rowsPerPage))
        setShowTypes(filterData.slice((currentPage - 1) * rowsPerPage, currentPage * rowsPerPage));



    }, [currentPage, rowsPerPage, refreshPage, allTypes,filterData])//


    const changePage = (event, value) => {
        setCurrentPage(value)
    }
    const changeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value))
    }
    //bấm các nút để hiện modal
    const onDetailBtnClick = (paramType) => {
        setDetailModalDisPlay(!DetailModalDisplay)
        setDataOfRow(paramType)
    }
    const onDeleteBtnClick = (paramType) => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
        setDataOfRow(paramType)

    }
    const onBtnAddClick = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }

    //Xử lý khi đóng các modal components
    const closeDetailModal = () => {
        setDetailModalDisPlay(!DetailModalDisplay)
    }
    const closeDeleteModal = () => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
    }
    const closeAddModal = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    //Xử lý update order
    const handleUpdateType = (paramData) => {
        let body = {
            method: 'PUT',
            body: JSON.stringify(paramData),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
        };

        getData("http://localhost:8000/productType/" + paramData._id, body)
            .then((data) => {
                closeDetailModal();
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã sửa order")
                setRefreshPage(!refreshPage)
            })


    }
    //Xử lý delete Order
    const handleDeleteType = (paramType) => {
        let body = {
            method: 'DELETE',
        };
        getData("http://localhost:8000/productType/" + paramType._id, body)
            .then((data) => {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã xoá nhóm sản phẩm")
                closeDeleteModal();
                setRefreshPage(!refreshPage)
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu xoá thất bại!")
            })
    }

    //Xử lý add Order
    const handleAddType = (paramType) => {
        const body = {
            method: 'POST',
            body: JSON.stringify(paramType),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("http://localhost:8000/productType", body)
            .then((data) => {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã thêm order")
                closeAddModal();
                setRefreshPage(!refreshPage)
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu thêm thất bại!")
            })

    }

    return (
        <div >
            <Grid container>
                <Grid item xs={12}>
                    <>
                        <Grid container className="mb-3">
                            <Grid item xs={2}>
                                <label >Show
                                    <input style={{ width: "5rem", textAlign: "center" }} type={"number"} value={rowsPerPage} onChange={changeRowsPerPage}>
                                    </input> entries
                                </label>
                            </Grid>

                            <Grid item xs={8} justifyItems="center" >
                                <Grid container direction="row" justifyContent="center" >
                                    <Typography style={{ width: "7%", marginRight: "2rem", marginTop:"4px" }}>Bộ lọc</Typography>
                                    <TextField
                                        label="tìm theo trường" select size="small" value={filter.field} style={{ width: "20%", marginRight: "2rem" }}
                                        onChange={(e) => setFilter({ ...filter, field: e.target.value })}>
                                        <MenuItem value={0} >Tắt bộ lọc </MenuItem>
                                        <MenuItem value="id">id</MenuItem>
                                        <MenuItem value="name">Tên</MenuItem>

                                    </TextField>
                                    <TextField placeholder="điền thông tin cần tìm" size="small" style={{ width: "25%" }} 
                                    value={filter.data}
                                    onChange={(e) => setFilter({ ...filter, data: e.target.value })}>
                                    </TextField>
                                </Grid>
                            </Grid>

                            <Grid item xs={2} style={{ justifyContent: "flex-end" }}>
                                <Button variant="contained" color="success" onClick={onBtnAddClick} >Add Order</Button>
                            </Grid>
                        </Grid>
                        <Grid>
                            <Grid item>
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="right">Tên</TableCell>
                                                <TableCell align="right">Mô tả</TableCell>
                                                <TableCell align="center">Chi tiết</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {showTypes.map((type, index) => (
                                                <TableRow
                                                    key={index}
                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                >

                                                    <TableCell align="right">{type.name}</TableCell>
                                                    <TableCell align="right">{type.description}</TableCell>
                                                    <TableCell align="center" >
                                                        <Button variant="contained" color="info" onClick={() => onDetailBtnClick(type)}>Detail</Button>
                                                        <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => onDeleteBtnClick(type)}>Delete</Button>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>
                        <Grid container mt={3} mb={2} justifyContent={"flex-end"}>
                            <Grid item>
                                <Pagination count={noPage} variant="outlined" onChange={changePage} />
                            </Grid>
                        </Grid>
                    </>
                </Grid>
            </Grid>

            <DetailModal isOpen={DetailModalDisplay} handleCloseModal={closeDetailModal} data={dataOfRow} updateType={handleUpdateType}></DetailModal>
            <DeleteModal isOpen={DeleteModalDisplay} handleCloseModal={closeDeleteModal} data={dataOfRow} deleteProductType={handleDeleteType}></DeleteModal>
            <AddModal isOpen={AddModalDisplay} handleCloseModal={closeAddModal} addProductType={handleAddType}></AddModal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
