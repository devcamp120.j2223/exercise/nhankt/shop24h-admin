
import { Modal, Box, Typography, Select, MenuItem, Button, CardMedia, TextField, Grid } from "@mui/material";
import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";
import { Label } from "reactstrap"


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 750,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 3.5,
};


function DetailModal({ isOpen, handleCloseModal, data, updateOrder, types }) {
    const [display, setDisplay] = useState(isOpen)
    const [detailData, setDetailData] = useState(data)
    const [refreshPage, setRefreshPage] = useState(Boolean(true))
    useEffect(() => {
        setDisplay(isOpen)
        setDetailData(data)
    }, [isOpen])
   
    const handleChangeCost = (event) => {
        const { formattedValue, value } = event;
        setDetailData(
           {...detailData, cost: value } 
        )
    }
    const handleChangeOrderStatus = (event) => {
        setDetailData(
           {...detailData, orderStatus: event.target.value } 
        )
    }
    const handleChangeName = (event,index1) => {
        setDetailData(
           {...detailData, orderDetail: detailData.orderDetail.map((e,i)=>i==index1?{...e,name: event.target.value }:e) } 
        )
    }
    const handleChangeQuantity = (event,index1) => {
        setDetailData(
           {...detailData, orderDetail: detailData.orderDetail.map((e,i)=>i==index1?{...e,quantity: event.target.value }:e) } 
        )
    }
    const handleChangeCity = (event) => {
        setDetailData(
           {...detailData, shippingAddress: {...detailData.shippingAddress,city:event.target.value}  } 
        )
    }
    const handleChangeDistrict = (event) => {
        setDetailData(
           {...detailData, shippingAddress: {...detailData.shippingAddress,district:event.target.value}  } 
        )
    }
    const handleChangeWard = (event) => {
        setDetailData(
           {...detailData, shippingAddress: {...detailData.shippingAddress,ward:event.target.value}  } 
        )
    }
    const handleChangeStreet = (event) => {
        setDetailData(
           {...detailData, shippingAddress: {...detailData.shippingAddress,street:event.target.value}  } 
        )
    }
   
    
    return (
        <div>
        <Modal
            open={display}
            onClose={() => { setDisplay(!display); handleCloseModal() }}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            width="400px"
        >
            <Box onSubmit={(e) => { e.preventDefault(); updateOrder(detailData) }} sx={style} component="form">
            <Typography  id="modal-modal-title" variant="h6" component="h2">
                    Chi tiết Order
                </Typography>
                <Grid container mb={2} style={{ marginLeft: "5px"  }}>
                    <Grid item xs={6}  >
                        <Grid><Label>Id</Label></Grid>
                        <Grid >
                            <TextField size="small" style={{ width: "90%" }} value= {detailData._id} disabled>
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid item xs={6}  >
                        <Grid><Label>Ngày tạo đơn</Label></Grid>
                        <Grid >
                            <TextField size="small" value={detailData.timeCreated} disabled style={{ width: "90%" }} >
                            </TextField>
                        </Grid>
                    </Grid>
                </Grid>
               
                <Grid container mb={2} style={{ marginLeft: "5px" }}>
                    <Grid item xs={6}>
                        <Grid><Label>Tình trạng đơn hàng</Label></Grid>
                        <Grid ><TextField size="small"
                            select
                            value={detailData.orderStatus }
                            style={{ width: "90%" }}
                            onChange={handleChangeOrderStatus}
                        >
                            <MenuItem value="cancel">Đã huỷ</MenuItem>
                            <MenuItem value="open">Chờ xác nhận</MenuItem>
                            <MenuItem value="confirm">Đã xác nhận</MenuItem>
                            <MenuItem value="shipping">Đang giao hàng</MenuItem>
                            <MenuItem value="done">Đã hoàn thành</MenuItem>
                        </TextField></Grid>
                    </Grid>
                    <Grid item xs={6}>
                        <Grid><Label>Giá tiền</Label></Grid>
                        <Grid><NumberFormat disabled required onValueChange={handleChangeCost} thousandSeparator={true} value={detailData.cost} style={{ width: "90%", height: "2.3rem" }}></NumberFormat></Grid>
                    </Grid>
                </Grid>
                <Grid container mb={1} style={{ marginLeft: "5px" }}>
                    <Grid item xs={8}>
                        <Grid><Label>Tên sản phẩm</Label></Grid>
                    </Grid>
                    <Grid item xs={4}>
                        <Grid><Label>Số lượng</Label></Grid>
                    </Grid>
                </Grid>
                {Object.keys(detailData).length>0?detailData.orderDetail.map((el, index) => 
                     (<Grid key={index} container mb={2} style={{ marginLeft: "5px" }}>
                        <Grid item xs={8}>
                            <Grid><TextField disabled required size="small" value={el.name} onChange={(e)=>handleChangeName(e,index)} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={4}>
                            <Grid><TextField disabled required size="small" type="number" value={el.quantity} onChange={(e)=>handleChangeQuantity(e,index)} style={{ width: "85%" }}></TextField></Grid>
                        </Grid>
                    </Grid>)
                ):null }
                <Typography  id="modal-modal-title" variant="h6" component="h2">
                Địa chỉ giao hàng
                </Typography>
                {Object.keys(detailData).length>0?
                <>
                <Grid container mb={2} style={{ marginLeft: "5px" }}>
                    <Grid item xs={6}>
                    <Grid><Label>Thành Phố</Label></Grid>
                    <Grid><TextField disabled required size="small" value={detailData.shippingAddress.city } onChange={handleChangeCity} style={{ width: "90%" }}></TextField></Grid>
                    </Grid>
                    <Grid item xs={6}>
                        <Grid><Label>Quận/huyện</Label></Grid>
                        <Grid><TextField disabled required size="small" value={detailData.shippingAddress.district } onChange={handleChangeDistrict} style={{ width: "90%" }}></TextField></Grid>
                    </Grid>
                </Grid>
                <Grid container mb={2} style={{ marginLeft: "5px" }}>
                    <Grid item xs={6}>
                    <Grid><Label>Phường</Label></Grid>
                    <Grid><TextField disabled required size="small" value={detailData.shippingAddress.ward } onChange={handleChangeWard} style={{ width: "90%" }}></TextField></Grid>
                    </Grid>
                    <Grid item xs={6}>
                        <Grid><Label>Đường, số nhà</Label></Grid>
                        <Grid><TextField disabled required size="small" value={detailData.shippingAddress.street } onChange={handleChangeStreet} style={{ width: "90%" }}></TextField></Grid>
                    </Grid>
                </Grid>
                </>
                :null}
                 
                <Grid container>
                    <Grid item xs={4} mx="auto" >
                        <Button variant="contained" color="info" type="submit" >Confrim</Button>
                        <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { handleCloseModal() }}>Cancel</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
        
    </div>
    );
}
export default DetailModal