
import { Modal, Box, Typography, Select, MenuItem, Button, Snackbar, Alert, Grid, TextField } from "@mui/material";
import { set } from "mongoose";
import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';

import { Col, Row, Label, Input } from "reactstrap"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    maxHeight:'90%',
    overflow:'scroll',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    
};
const style2 = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
const DAY_AFTER_CONFIRMED_ORDER = 5
const citysArray = require(`../../assets/locations/cities.json`).data
function AddModal({ isOpen, handleCloseModal, addOrder }) {

    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }

    let vNewOrder = {
        orderDetail: [],
        cost: 0  
    }
    const [display, setDisplay] = useState(isOpen)
    const [display2, setDisplay2] = useState(false)
    const [newOrder, setNewOrder] = useState(vNewOrder)
    const [allProducts, setAllProducts] = useState([])
    const [allType, setAllTypes] = useState([])
    const [allCustomer, setAllCustomer] = useState([])
    const [selectedProduct, setSelectedProduct] = useState("")
    const [selectedType, setSelectedType] = useState("")
    const [selectedCustomer, setSelectedCustomer] = useState("")
    const [indexOfDeleteProduct, setIndexOfDeleteProduct] = useState("")
    const [selectedCity, setSelectedCity] = useState("")
    const [selectedDistrict, setSelectedDistrict] = useState("")
    const [selectedWard, setSelectedWard] = useState("")
    const [street, setStreet] = useState("")
    const [districtsArray, setDistrictsArray] = useState([])
    const [wardsArray, setWardsArray] = useState([])
    const [note, setNote] = useState("")



    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    useEffect(() => {
        getData("http://localhost:8000/productType")
            .then(res => setAllTypes(res.data))
        getData("http://localhost:8000/product")
            .then(res => setAllProducts(res.data))
        getData("http://localhost:8000/customer")
            .then(res => setAllCustomer(res.data))
    }, [])

    useEffect(() => {
        setNewOrder(vNewOrder)
        setDisplay(isOpen)
        setSelectedCustomer("")
        setSelectedType("")
    }, [isOpen])
    useEffect(() => {
        let cost1 = 20000
        newOrder.orderDetail.forEach(el => cost1 += allProducts.filter(e => e._id == el.productId)[0].promotionPrice * el.quantity)
        setNewOrder({ ...newOrder, cost: cost1 })
    }, [newOrder.orderDetail,newOrder.orderDetail.length])
    useEffect(() => {
        //console.log(selectedCity)
        if (selectedCity != "") {
            let newDistrictArray = require(`../../assets/locations/districts/${selectedCity}.json`).data
            setDistrictsArray(newDistrictArray)
            //console.log(newDistrictArray)
        }
        if (selectedDistrict != "") {
            let newWardArray = require(`../../assets/locations/wards/${selectedDistrict}.json`).data
            setWardsArray(newWardArray)
            //console.log(newWardArray)
        }

    },[selectedCity,selectedDistrict])


    const checkValidOrder = (paramOrder) => {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (newOrder.orderDetail.length<1) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Vui lòng thêm sản phẩm để tạo đơn hàng")
            return false
        }

        return true
    }
    const handleConfirmBtn = (e) => {
        e.preventDefault();
        let isValid = checkValidOrder(newOrder)
        if (isValid) {
            let shippedDate = new Date();
            shippedDate.setDate(shippedDate.getDate() + DAY_AFTER_CONFIRMED_ORDER);
            const order = {
                shippedDate: shippedDate,
                orderDetail: newOrder.orderDetail,
                shippingAddress: {
                    city: citysArray.filter(el => el.id == selectedCity)[0].name,
                    district: districtsArray.filter(el => el.id == selectedDistrict)[0].name,
                    ward: wardsArray.filter(el => el.id == selectedWard)[0].name,
                    street: street
                },
                cost: newOrder.cost,
                note: note
            }
            const customerId = allCustomer[selectedCustomer]._id
            addOrder({order:order,customerId:customerId})
        }

    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }


    const handleChangeCustomer = (e) => {
        setSelectedCustomer(e.target.value)
    }
    const handleChangeType = (e) => {
        setSelectedType(e.target.value)
        setSelectedProduct("")
    }
    const handleChangeProduct = (e) => {
        setSelectedProduct(e.target.value)
    }
    const handleAddProduct = () => {
        let addProduct = allProducts.filter(el => el.type == allType[selectedType]._id)[selectedProduct]
        var oldOrderDetail = newOrder.orderDetail
        let addToOrder = { productId: addProduct._id, quantity: 1, name: addProduct.name }
        oldOrderDetail.push(addToOrder)
        setNewOrder({ ...newOrder, orderDetail: oldOrderDetail })

    }
    const decreaseQuantity = (index) => {
        if (newOrder.orderDetail[index].quantity == 1) {
            setDisplay2(true)
            setIndexOfDeleteProduct(index)

        }
        else {
            let newOrderDetail = newOrder.orderDetail.map((el, i) => i == index ? { ...el, quantity: el.quantity - 1 } : el)
            setNewOrder({ ...newOrder, orderDetail: newOrderDetail })
        }

    }
    const increaseQuantity = (index) => {
        let newOrderDetail = newOrder.orderDetail.map((el, i) => i == index ? { ...el, quantity: el.quantity + 1 } : el)
        setNewOrder({ ...newOrder, orderDetail: newOrderDetail })
    }
    const deleteProduct = () => {
        let newOrderDetail = newOrder.orderDetail.filter((el, index) => index != indexOfDeleteProduct)
        setNewOrder({ ...newOrder, orderDetail: newOrderDetail })
        setDisplay2(false)
        setOpenAlert(true);
        setStatusModal("error")
        setNoidungAlertValid("bạn đã xoá sản phẩm khỏi đơn hàng")
    }
    const handleChangeSelectedCity = (event) => {
        setSelectedCity(event.target.value)
    }
    const handleChangeSelectedDistrict = (event) => {
        setSelectedDistrict(event.target.value)
    }
    const handleChangeSelectedWard = (event) => {
        setSelectedWard(event.target.value)
    }
    const handleStreet = (event) => {
        setStreet(event.target.value)
    }
    return (
        <div>
            <Modal
                open={display}
                onClose={() => { setDisplay(!display); handleCloseModal() }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box onSubmit={handleConfirmBtn} sx={style} component="form">
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Thêm đơn hàng mới
                    </Typography>
                    <Grid container mb={2} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}>
                            <Grid><Label> Tên Khách hàng </Label></Grid>
                            <Grid >
                                <TextField size="small"
                                    required
                                    select
                                    value={selectedCustomer}
                                    style={{ width: "90%" }}
                                    onChange={handleChangeCustomer}
                                >
                                    {allCustomer.map((el, index) => <MenuItem key={index} value={index}>{(el.fullName)}</MenuItem>)}
                                </TextField>
                            </Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label> Số điện thoại </Label></Grid>
                            <Grid >
                                <TextField size="small"
                                    required
                                    select
                                    value={selectedCustomer}
                                    style={{ width: "90%" }}
                                    onChange={handleChangeCustomer}
                                >
                                    {allCustomer.map((el, index) => <MenuItem key={index} value={index}>{(el.phone)}</MenuItem>)}
                                </TextField>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container mb={0} style={{ marginLeft: "5px" }}>
                        <Grid item xs={4}>
                            <Grid><Label> Nhóm sản phẩm </Label></Grid>
                            <Grid >
                                <TextField size="small"
                                    required
                                    select
                                    value={selectedType}
                                    style={{ width: "90%" }}
                                    onChange={handleChangeType}
                                >
                                    {allType.length > 0 ? allType.map((el, index) => <MenuItem key={index} value={index}>{(el.name)}</MenuItem>) : []}
                                </TextField>
                            </Grid>
                        </Grid>
                        <Grid item xs={8}>
                            <Grid><Label> Sản phẩm </Label></Grid>
                            <Grid container>
                                <Grid item xs={9}>
                                    <TextField size="small"
                                        required
                                        select
                                        value={selectedProduct}
                                        style={{ width: "90%" }}
                                        onChange={handleChangeProduct}
                                    >
                                        {selectedType !== "" ? allProducts.filter(el => el.type == allType[selectedType]._id).map((el, index) => <MenuItem key={index} value={index}>{(el.name)}</MenuItem>) : []}

                                    </TextField>
                                </Grid>
                                <Grid item>
                                    <Button variant="contained" onClick={handleAddProduct}>Thêm</Button>
                                </Grid>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Grid container>
                        <Grid item xs={6} mx="auto" borderBottom={0.5} my={3}></Grid>
                    </Grid>

                    <Grid container mb={2}>

                        <Grid item xs={8}>
                            {newOrder.orderDetail.length > 0 ? <Grid container mb={2}>
                                <Typography id="modal-modal-title" variant="h6" component="h2">
                                    Danh sách sản phẩm chọn mua
                                </Typography>
                            </Grid> : null}

                            {newOrder.orderDetail.length > 0 ? newOrder.orderDetail.map((el, index) =>
                            (<Grid key={index} container mb={2} style={{ marginLeft: "5px" }}>
                                <Grid item xs={8}>
                                    <Grid><TextField disabled size="small" value={el.name} style={{ width: "90%" }}></TextField></Grid>
                                </Grid>

                                <Grid item xs={4}>
                                    <Typography>
                                        <Button onClick={() => { decreaseQuantity(index) }} ><RemoveCircleIcon style={{ color: "black" }} /></Button>
                                        {el.quantity}
                                        <Button onClick={() => { increaseQuantity(index) }}><AddCircleIcon style={{ color: "black" }} /></Button>
                                    </Typography>

                                </Grid>

                            </Grid>)
                            ) : null}
                        </Grid>{
                            newOrder.orderDetail.length > 0 ?
                                <Grid item xs={4}>
                                    <Typography mb={2} id="modal-modal-title" variant="h6" component="h2">
                                        Thành tiền
                                    </Typography>
                                    <NumberFormat disabled
                                        thousandSeparator={true} value={newOrder.cost} style={{ width: "90%", height: "2.3rem" }}>
                                    </NumberFormat>
                                </Grid> : ""
                        }
                    </Grid>
                    {
                        newOrder.orderDetail.length > 0 ?
                            <Grid>
                                <Grid mb={2} >
                                    <Typography>Địa chỉ giao hàng</Typography>
                                </Grid>
                                <Grid container mb={3.5} >
                                    <Grid item xs={5.5} mr={3}>
                                        <TextField
                                            required
                                            fullWidth
                                            size="small"
                                            label="Tỉnh/Thành Phố"
                                            select
                                            value={selectedCity}
                                            onChange={handleChangeSelectedCity}
                                        >
                                            {citysArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                                        </TextField>
                                    </Grid>
                                    <Grid item xs={5.5} ml={1}>
                                        <TextField
                                            required
                                            select
                                            fullWidth
                                            size="small"
                                            label="Quận/Huyện"
                                            value={selectedDistrict}
                                            onChange={handleChangeSelectedDistrict}
                                        >
                                            {districtsArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                                        </TextField>
                                    </Grid>
                                </Grid>
                                <Grid container mb={3.5}>
                                    <Grid item xs={5.5} >
                                        <TextField
                                            required
                                            select
                                            fullWidth
                                            size="small"
                                            label="Phường/Xã"
                                            value={selectedWard}
                                            onChange={handleChangeSelectedWard}
                                        >
                                            {wardsArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                                        </TextField>
                                    </Grid>
                                    <Grid item xs={5.5} ml={4} >
                                        <TextField
                                            required
                                            fullWidth
                                            size="small"
                                            label="Đường, số nhà"
                                            value={street}
                                            onChange={handleStreet}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container >
                                    <Grid item xs={11.6}>
                                        <TextField
                                            multiline
                                            fullWidth
                                            size="large"
                                            label="Ghi chú thêm"
                                            value={note}
                                            onChange={(event) => { setNote(event.target.value) }}
                                        />
                                    </Grid>
                                </Grid>

                            </Grid>
                            : ""}
                            <Grid container>
                        <Grid item xs={4} mx="auto" >
                            <Button variant="contained" color="info" type="submit" >Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { handleCloseModal() }}>Cancle</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Modal
                open={display2}
                onClose={() => { setDisplay2(false) }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style2}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {`Bạn chắc chắn muốn xoá  ???`}
                    </Typography>
                    <Row className="mt-4">
                        <Col sm="12" className="text-center" >
                            <Button variant="contained" color="info" onClick={deleteProduct}>Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { setDisplay2(false) }}>Cancle</Button>
                        </Col>
                    </Row>
                </Box>
            </Modal>

            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
export default AddModal