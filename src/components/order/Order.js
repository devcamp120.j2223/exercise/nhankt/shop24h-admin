import { Button, Container, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Pagination, Snackbar, CardMedia, Alert, Typography, TextField, MenuItem, } from "@mui/material"
import { useEffect, useState } from "react";
import AppSidebar from "../AppSidebar";
import Header from "../Header";
import AddModal from "./addModal";
import DeleteModal from "./deleteModal";
import DetailModal from "./detailModal";
import NumberFormat from "react-number-format";



export default function Order() {
    const [rowsPerPage, setRowsPerPage] = useState(8);
    const [orders, setOrders] = useState([])
    const [showOrders, setShowOrders] = useState([])
    const [noPage, setNoPage] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [refreshPage, setRefreshPage] = useState(Boolean(true))
    const [filter, setFilter] = useState({ field: 0, data: "" })
    const [customers,setCustomers] = useState([])
    const [filterData, setFilterData] = useState([])
    //state hiển thị các modal
    const [DetailModalDisplay, setDetailModalDisPlay] = useState(Boolean(false))
    const [DeleteModalDisplay, setDeleteModalDisPlay] = useState(Boolean(false))
    const [AddModalDisplay, setAddModalDisPlay] = useState(Boolean(false))
    //state dữ liệu khi ấn các nút của 1 order
    const [dataOfRow, setDataOfRow] = useState({})
    //state thông báo
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    //fetch
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    //sự kiện load trang, tải dữ liệu
    useEffect(() => {
        getData("http://localhost:8000/orders")
            .then((data) => {
                setOrders(data.data)
                setFilterData(data.data)
            })
        getData("http://localhost:8000/customer")
            .then((data) => {
                setCustomers(data.data)
            })
    }, [refreshPage])
    useEffect(() => {
        if (filter.field == "id") {
            setFilterData(orders.filter(el => (el._id).toLowerCase().includes(filter.data)))
        }
        if (filter.field == "name") {
            setFilterData(orders.filter(el =>{
                let vCustomers = customers.filter(el1=>el1.fullName.toLowerCase().includes(filter.data))
                for ( let el3 of vCustomers) {
                    if (el3._id == el.customerId){
                        return true
                    }
                }
                return false
            }))
        }
        else if (filter.field == "city") {
            setFilterData(orders.filter(el => (el.shippingAddress.city).toLowerCase().includes(filter.data)))
        }
        else if (filter.field == "cost") {
            if (filter.data != "") {
                setFilterData(orders.filter(el => parseInt(el.cost) >= parseInt(filter.data)))
            }
            else { setFilterData(orders) }
        }
        else { setFilterData(orders) }
    }, [filter])
    useEffect(() => {
        setFilter({ ...filter, data: "" })
    }, [filter.field])
    useEffect(() => {

        setNoPage(Math.ceil(filterData.length / rowsPerPage))
        setShowOrders(filterData.slice((currentPage - 1) * rowsPerPage, currentPage * rowsPerPage));

    }, [currentPage, rowsPerPage, refreshPage, orders, filterData])//

    const changePage = (event, value) => {
        setCurrentPage(value)
    }
    const changeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value))
    }
    //bấm các nút để hiện modal
    const onDetailBtnClick = (order) => {
        setDetailModalDisPlay(true)
        setDataOfRow(order)
    }
    const onDeleteBtnClick = (paramOrder) => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
        setDataOfRow(paramOrder)

    }
    const onBtnAddClick = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }

    //Xử lý khi đóng các modal components
    const closeDetailModal = () => {
        setDetailModalDisPlay(!DetailModalDisplay)
    }
    const closeDeleteModal = () => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
    }
    const closeAddModal = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    //Xử lý update order
    const handleUpdateOrder = (paramOrder) => {
        let body = {
            method: 'PUT',
            body: JSON.stringify(
                paramOrder
            ),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
        };

        getData("http://localhost:8000/orders/" + paramOrder._id, body)
            .then((data) => {
                closeDetailModal();
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã sửa order")
                setRefreshPage(!refreshPage)
            })


    }
    //Xử lý delete Order
    const handleDeleteOrder = (paramOrder) => {
        let body = {
            method: 'DELETE',
        };
        getData("http://localhost:8000/orders/" + paramOrder._id, body)
            .then((data) => {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã xoá nhóm đơn hàng")
                closeDeleteModal();
                setRefreshPage(!refreshPage)
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("xoá thất bại!")
            })
    }

    //Xử lý add Order
    const handleAddOrder = (paramData) => {


        const body = {
            method: 'POST',
            body: JSON.stringify(paramData.order),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData(`http://localhost:8000/customer/${paramData.customerId}/orders`, body)
            .then((data) => {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã thêm order")
                closeAddModal();
                setRefreshPage(!refreshPage)
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu thêm thất bại!")
            })

    }

    return (
        <div >
            <Grid container>
                <Grid item xs={12}>
                    <>
                        <Grid container className="mb-3">
                            <Grid item xs={2}>
                                <label >Show
                                    <input style={{ width: "5rem", textAlign: "center" }} type={"number"} value={rowsPerPage} onChange={changeRowsPerPage}>
                                    </input> entries
                                </label>
                            </Grid>
                            <Grid item xs={8} justifyItems="center" >
                                <Grid container direction="row" justifyContent="center" >
                                    <Typography style={{ width: "7%", marginRight: "2rem", marginTop: "4px" }}>Bộ lọc</Typography>
                                    <TextField
                                        label="tìm theo trường" select size="small" value={filter.field} style={{ width: "20%", marginRight: "2rem" }}
                                        onChange={(e) => setFilter({ ...filter, field: e.target.value })}>
                                        <MenuItem value={0} >Tắt bộ lọc </MenuItem>
                                        <MenuItem value="id">id</MenuItem>
                                        <MenuItem value="name">Tên khách hàng</MenuItem>
                                        <MenuItem value="city">Thành phố</MenuItem>
                                        <MenuItem value="cost">Tổng tiền</MenuItem>
                                    </TextField>
                                    {filter.field == "cost" ?
                                        <NumberFormat placeholder="Giá min" size="small" style={{ width: "15%", marginRight: "1rem" }}
                                            thousandSeparator={true} value={filter.data}
                                            onValueChange={(e) => { const { formattedValue, value } = e; setFilter({ ...filter, data: value }) }}>
                                        </NumberFormat>
                                        : <TextField placeholder="điền thông tin cần tìm" size="small" style={{ width: "25%" }} value={filter.data}
                                            onChange={(e) => setFilter({ ...filter, data: e.target.value })}>
                                        </TextField>}
                                </Grid>
                            </Grid>



                            <Grid item xs={2} style={{ justifyContent: "flex-end" }}>
                                <Button variant="contained" color="success" onClick={onBtnAddClick} >Add Order</Button>
                            </Grid>
                        </Grid>
                        <Grid>
                            <Grid item>
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="right" >Tên khách hàng</TableCell>
                                                <TableCell align="right" width="100px">địa chỉ giao</TableCell>
                                                <TableCell align="right">ngày giao</TableCell>
                                                <TableCell align="right">Tình trạng</TableCell>
                                                <TableCell align="right">sản phẩm</TableCell>
                                                <TableCell align="right">tổng tiền</TableCell>
                                                <TableCell align="center">Chi tiết</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {showOrders.map((order, index) => (
                                                <TableRow
                                                    key={index}
                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                >
                                                    <TableCell align="right" >{customers.length>0?customers.filter(el=>el._id==order.customerId)[0].fullName:"" }</TableCell>
                                                    
                                                    <TableCell align="right">{order.shippingAddress.city + " , " + order.shippingAddress.district}</TableCell>
                                                    <TableCell align="right">{`${order.shippedDate.slice(0, 4)}-${order.shippedDate.slice(5, 7)}-${order.shippedDate.slice(8, 10)}`}</TableCell>
                                                    <TableCell align="right">{order.orderStatus}</TableCell>
                                                    <TableCell align="right">{order.orderDetail.map((el, index) => (
                                                        <Typography key={index}>{el.name} - {el.quantity}</Typography>
                                                    )
                                                    )}

                                                    </TableCell>
                                                    <TableCell align="right">{order.cost.toLocaleString()}</TableCell>
                                                    <TableCell align="center" >
                                                        <Button variant="contained" color="info" onClick={() => onDetailBtnClick(order)}>Detail</Button>
                                                        <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => onDeleteBtnClick(order)}>Delete</Button>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>
                        <Grid container mt={3} mb={2} justifyContent={"flex-end"}>
                            <Grid item>
                                <Pagination count={noPage} variant="outlined" onChange={changePage} />
                            </Grid>
                        </Grid>

                    </>
                </Grid>
            </Grid>


            <DetailModal isOpen={DetailModalDisplay} handleCloseModal={closeDetailModal} data={dataOfRow} updateOrder={handleUpdateOrder}></DetailModal>
            <DeleteModal isOpen={DeleteModalDisplay} handleCloseModal={closeDeleteModal} data={dataOfRow} deleteOrder={handleDeleteOrder}></DeleteModal>
            <AddModal isOpen={AddModalDisplay} handleCloseModal={closeAddModal} addOrder={handleAddOrder}></AddModal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
