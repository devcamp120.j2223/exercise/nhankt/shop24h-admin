import { CHeaderToggler, CHeader, CContainer,CHeaderDivider } from "@coreui/react";
import CIcon from '@coreui/icons-react'
import { useSelector, useDispatch } from 'react-redux'
import { cilMenu } from '@coreui/icons'
export default function Header() {
    const dispatch = useDispatch()
    const sidebarShow = useSelector((state) => state.sidebarShow)
    return (
        <CHeader position="sticky" className="mb-4">
            <CContainer fluid>
                <CHeaderToggler
                    className="ps-1"
                    onClick={() => dispatch({ type: 'set', sidebarShow: !sidebarShow })}
                >
                    <CIcon icon={cilMenu} size="lg" />
                </CHeaderToggler>
            </CContainer>
        </CHeader>
    )
}