
import { Modal, Box, Typography, Select, MenuItem, Button, Snackbar, Alert,Grid,TextField } from "@mui/material";
import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";

import { Col, Row, Label, Input } from "reactstrap"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


function AddModal({ isOpen, handleCloseModal, addProduct,types }) {
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }

    var vNewProduct = {
        name: "",
        imageUrl: "",
        buyPrice: "",
        promotionPrice: "",
        amount: "",
        type:""
    }
    const [display, setDisplay] = useState(isOpen)
    const [newProduct, setNewProduct] = useState(vNewProduct)
    const [detailTypes, setDetailTypes] = useState(types)
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    useEffect(() => {
        setDisplay(isOpen)
        setDetailTypes(types)
    }, [isOpen])
    
    
    const checkValidOrder = (paramOrder) => {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (paramOrder==""){
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("test")
            return false
        }
       
        return true
    }
    const handleConfirmBtn = (e) => {
        let isValid = checkValidOrder(newProduct)
        if (isValid) {
           e.preventDefault();
           addProduct(newProduct)
        }

    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    useEffect(() => {
        setNewProduct(vNewProduct)
        setDisplay(isOpen)
    }, [isOpen])
    const handleChangeName = (event) => {
        setNewProduct({
            ...newProduct,
            name: event.target.value
        })
    }
    const handleChangeType = (event) => {
        setNewProduct({
            ...newProduct,
            type: detailTypes[event.target.value]._id
        })
    }
    const handleChangeBuyPrice = (event) => {
        const { formattedValue, value } = event;
        setNewProduct({
            ...newProduct,
            buyPrice: value
        })
    }
    const handleChangePromotionPrice = (event) => {
        const { formattedValue, value } = event;
        setNewProduct({
            ...newProduct,
            promotionPrice: value
        })
    }
    const handleChangeImageUrl = (event) => {
        setNewProduct({
            ...newProduct,
            imageUrl: event.target.value
        })
    }
    const handleChangeDescription = (event) => {
        setNewProduct({
            ...newProduct,
            description: event.target.value
        })
    }
    const handleChangeAmount = (event) => {
        setNewProduct({
            ...newProduct,
            amount: event.target.value
        })
    }
    
    return (
        <div>
            <Modal
                open={display}
                onClose={() => { setDisplay(!display); handleCloseModal() }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box onSubmit={handleConfirmBtn} sx={style} component="form">
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Thêm sản phẩm mới
                    </Typography>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}>
                            <Grid><Label>Tên sản phẩm</Label></Grid>
                            <Grid ><TextField onChange={handleChangeName} size="small" value={newProduct.name} style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                        <Grid><Label>Số lượng</Label></Grid>
                            <Grid><TextField size="small" style={{ width: "90%" }} value={newProduct.amount} onChange={handleChangeAmount}></TextField> </Grid>
                        </Grid>
                    </Grid>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>TypeId</Label></Grid>
                            <Grid ><TextField size="small" disabled value={newProduct.type} readOnly style={{ width: "90%" }}></TextField></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Tên nhóm</Label></Grid>
                            <Grid >
                                <TextField size="small"
                                    select
                                    value={newProduct.type!=""?detailTypes.map(el => el._id).indexOf(newProduct.type):""}
                                    style={{ width: "90%" }}
                                    onChange={handleChangeType}
                                >
                                    {detailTypes.map((el, index) => <MenuItem key={index} value={index}>{el.name}</MenuItem>)}
                                </TextField>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container mb={4} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Giá niêm yết</Label></Grid>
                            <Grid ><NumberFormat onValueChange={handleChangeBuyPrice} thousandSeparator={true} value={newProduct.buyPrice} style={{ width: "90%", height: "2.3rem" }}></NumberFormat></Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid><Label>Giá khuyến mãi</Label></Grid>
                            <Grid ><NumberFormat onValueChange={handleChangePromotionPrice} thousandSeparator={true} value={newProduct.promotionPrice} style={{ width: "90%", height: "2.3rem" }}></NumberFormat></Grid>
                        </Grid>
                    </Grid>
                    <Grid container mb={2} style={{ marginLeft: "5px" }}>
                        <Grid item xs={6}  >
                            <Grid><Label>Url Ảnh</Label></Grid>
                            <Grid><textarea onChange={handleChangeImageUrl} value={newProduct.imageUrl} style={{ width: "90%" }}></textarea></Grid>
                            <img src={newProduct.imageUrl} height="200px"></img>
                        </Grid>
                        <Grid item xs={6}  >
                            <Grid><Label>Mô tả</Label></Grid>
                            <Grid><textarea onChange={handleChangeDescription} rows={8} value={newProduct.description} style={{ width: "90%" }}></textarea></Grid>

                        </Grid>
                    </Grid>
                    

                    <Grid container>
                        <Grid item xs={4} mx="auto" >
                            <Button variant="contained" color="info" type="submit" >Confrim</Button>
                            <Button variant="contained" color="error" style={{ marginLeft: "10px" }} onClick={() => { handleCloseModal() }}>Cancle</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
export default AddModal