import { Button, Container, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Pagination, Snackbar, CardMedia, Alert, TextField, MenuItem, Typography } from "@mui/material"
import React, { Suspense, useEffect, useState } from "react";
import NumberFormat from "react-number-format";

import AddModal from "./addModal";
import DeleteModal from "./deleteModal";
import DetailModal from "./detailModal";
import { CContainer, CSpinner } from '@coreui/react'






export default function Product() {
    const [rowsPerPage, setRowsPerPage] = useState(8);
    const [products, setProducts] = useState([])
    const [showProducts, setShowProducts] = useState([])
    const [types, setTypes] = useState([])
    const [noPage, setNoPage] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [refreshPage, setRefreshPage] = useState(Boolean(true))
    const [filter, setFilter] = useState({ field: 0, data: "", min: "", max: "" })
    const [filterData, setFilterData] = useState([])
    //state hiển thị các modal
    const [DetailModalDisplay, setDetailModalDisPlay] = useState(Boolean(false))
    const [DeleteModalDisplay, setDeleteModalDisPlay] = useState(Boolean(false))
    const [AddModalDisplay, setAddModalDisPlay] = useState(Boolean(false))
    //state dữ liệu khi ấn các nút của 1 product
    const [dataOfRow, setDataOfRow] = useState({})
    //state thông báo
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    //fetch
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    //sự kiện load trang, tải dữ liệu
    useEffect(() => {
        getData("http://localhost:8000/product")
            .then((data) => {
                setProducts(data.data)
                setFilterData(data.data)
            })
            .catch((er)=>{console.log(er)})

        getData("http://localhost:8000/productType")
            .then((res) => { setTypes(res.data) })
    }, [refreshPage])
    useEffect(()=>{
        setFilter({ ...filter, data:"",min:"",max:"" })
    },[filter.field])
    useEffect(() => {
        if (filter.field == "id") {
            setFilterData(products.filter(el => (el._id).toLowerCase().includes(filter.data)))
        }
        else if (filter.field == "name") {
            setFilterData(products.filter(el => (el.name).toLowerCase().includes(filter.data)))
        }
        else if (filter.field == "type") {
            setFilterData(products.filter(el => (types.filter(e => e._id == el.type)[0].name).toLowerCase().includes(filter.data)))
        }
        else if (filter.field == "price") {
            if (filter.min=="" && filter.min=="") {setFilterData(products)}
            else if (filter.min=="" ) {setFilterData(products.filter(el=>(parseInt(el.promotionPrice)<=parseInt(filter.max))))}
            else if (filter.max=="" ) {setFilterData(products.filter(el=>(parseInt(el.promotionPrice)>=parseInt(filter.min))))}
            else {
                if (parseInt(filter.min) >= parseInt(filter.max)) {
                    console.log(filter)
                    setFilterData(products.filter(el=>(parseInt(el.promotionPrice)>=parseInt(filter.min))))
                }
                else {
                    setFilterData(products.filter(el=>(parseInt(el.promotionPrice)>=parseInt(filter.min) && parseInt(el.promotionPrice)<=parseInt(filter.max))))
                }
            }
        }
        else { setFilterData(products) }
    }, [filter])
    
    useEffect(() => {
        setNoPage(Math.ceil(filterData.length / rowsPerPage))
        setShowProducts(filterData.slice((currentPage - 1) * rowsPerPage, currentPage * rowsPerPage));
    }, [currentPage, rowsPerPage, refreshPage, products, filterData,types])//
    
  
    const changePage = (event, value) => {
        setCurrentPage(value)
    }
    const changeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value))
    }
    //bấm các nút để hiện modal
    const onDetailBtnClick = (product) => {
        setDetailModalDisPlay(!DetailModalDisplay)
        setDataOfRow(product)
        //setTypes(types)
    }
    const onDeleteBtnClick = (product) => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
        setDataOfRow(product)
    }
    const onBtnAddClick = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }

    //Xử lý khi đóng các modal components
    const closeDetailModal = () => {
        setDetailModalDisPlay(!DetailModalDisplay)
    }
    const closeDeleteModal = () => {
        setDeleteModalDisPlay(!DeleteModalDisplay)
    }
    const closeAddModal = () => {
        setAddModalDisPlay(!AddModalDisplay)
    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    //Xử lý update product
    const handleUpdateProduct = (paramData) => {
        console.log(paramData)
        let body = {
            method: 'PUT',
            body: JSON.stringify(
                paramData
            ),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
        };

        getData("http://localhost:8000/product/" + paramData._id, body)
            .then((data) => {
                closeDetailModal();
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã sửa product")
                setRefreshPage(!refreshPage)
            })


    }
    //Xử lý delete product
    const handleDeleteProduct = (paramData) => {
        let body = {
            method: 'DELETE',
        };
        const deleteProduct = async (body) => {
            const response = await fetch("http://localhost:8000/product/" + paramData._id, body);
            closeDeleteModal();
            setOpenAlert(true);
            setStatusModal("warning")
            setNoidungAlertValid("đã xoá Product")
            setRefreshPage(!refreshPage)
        }
        deleteProduct(body)
    }

    //Xử lý add Product
    const handleAddProduct = (paramProduct) => {
        console.log(paramProduct)
        const body = {
            method: 'POST',
            body: JSON.stringify(paramProduct),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("http://localhost:8000/product/", body)
            .then((data) => {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã thêm Product")
                closeAddModal();
                setRefreshPage(!refreshPage)
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu thêm thất bại!")
            })

    }

    return (
        <div >
            <Grid container>
                <Grid item xs={12}>
                    <>
                        <Grid container className="mb-3">
                            <Grid item xs={2}>
                                <label >Show
                                    <input style={{ width: "5rem", textAlign: "center" }} type={"number"} value={rowsPerPage} onChange={changeRowsPerPage}>
                                    </input > entries
                                </label>
                            </Grid>
                            <Grid item xs={8} justifyItems="center" >
                                <Grid container direction="row" justifyContent="center" >
                                    <Typography style={{ width: "7%", marginRight: "2rem", marginTop: "4px" }}>Bộ lọc</Typography>
                                    <TextField
                                        label="tìm theo trường" select size="small" value={filter.field} style={{ width: "20%", marginRight: "2rem", }}
                                        onChange={(e) => setFilter({ ...filter, field: e.target.value })}>
                                        <MenuItem value={0} >Tắt bộ lọc </MenuItem>
                                        <MenuItem value="id">id</MenuItem>
                                        <MenuItem value="name">Tên</MenuItem>
                                        <MenuItem value="type">Loại</MenuItem>
                                        <MenuItem value="price">Giá</MenuItem>

                                    </TextField>
                                    {filter.field == "price" ?
                                        <>
                                            <NumberFormat placeholder="Giá min" size="small" style={{ width: "15%", marginRight: "1rem" }}
                                                thousandSeparator={true} value={filter.min}
                                                onValueChange={(e) =>{const { formattedValue, value } = e;setFilter({ ...filter, min: value })} }>
                                            </NumberFormat>
                                            <NumberFormat placeholder="Giá max" size="small" style={{ width: "15%" }}
                                                thousandSeparator={true} value={filter.max}
                                                onValueChange={(e) =>{const { formattedValue, value } = e;setFilter({ ...filter, max: value })} }>
                                            </NumberFormat>
                                        </> : <TextField placeholder="điền thông tin cần tìm" size="small" style={{ width: "25%", }}
                                            value={filter.data}
                                            onChange={(e) => setFilter({ ...filter, data: e.target.value })}>
                                        </TextField>
                                    }

                                </Grid>
                            </Grid>
                            <Grid item xs={2} style={{ justifyContent: "flex-end" }}>
                                <Button variant="contained" color="success" onClick={onBtnAddClick} >Add Product</Button>
                            </Grid>
                        </Grid>
                        <Grid>
                            <Grid item>
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="left">Tên</TableCell>
                                                <TableCell align="left">Loại</TableCell>
                                                <TableCell align="right">Img</TableCell>
                                                <TableCell align="right">Giá</TableCell>
                                                <TableCell align="right">Giá khuyến mãi</TableCell>
                                                <TableCell align="right">Số lượng</TableCell>
                                                <TableCell align="center">Chi tiết</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {((showProducts.length>0)&&(types.length) > 0)?showProducts.map((product, index) => (
                                                <TableRow
                                                    key={index}
                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                >
                                                    <TableCell align="left">{product.name}</TableCell>
                                                    <TableCell align="left">{types.filter(el => el._id == product.type)[0].name}</TableCell>
                                                    <TableCell align="right">{<CardMedia
                                                        component="img"
                                                        height="40"
                                                        width="40"
                                                        image={product.imageUrl}
                                                        alt="green iguana"
                                                    />}</TableCell>
                                                    <TableCell align="right">{product.buyPrice.toLocaleString()}</TableCell>
                                                    <TableCell align="right">{product.promotionPrice.toLocaleString()}</TableCell>
                                                    <TableCell align="right">{product.amount}</TableCell>
                                                    <TableCell align="center" >
                                                        <Button variant="contained" color="info" size="small" onClick={() => onDetailBtnClick(product)}>Detail</Button>
                                                        <Button variant="contained" color="error" size="small" onClick={() => onDeleteBtnClick(product)}>Delete</Button>
                                                    </TableCell>
                                                </TableRow>
                                            )):null}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>
                        <Grid container mt={3} mb={2} justifyContent={"flex-end"}>
                            <Grid item>
                                <Pagination count={noPage} variant="outlined" onChange={changePage} />
                            </Grid>
                        </Grid>

                    </>
                </Grid>
            </Grid>


            <DetailModal isOpen={DetailModalDisplay} handleCloseModal={closeDetailModal} data={dataOfRow} types={types} updateProduct={handleUpdateProduct}></DetailModal>
            <DeleteModal isOpen={DeleteModalDisplay} handleCloseModal={closeDeleteModal} data={dataOfRow} deleteProduct={handleDeleteProduct} ></DeleteModal>
            <AddModal isOpen={AddModalDisplay} handleCloseModal={closeAddModal} addProduct={handleAddProduct} types={types}></AddModal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </div>
    );
}
