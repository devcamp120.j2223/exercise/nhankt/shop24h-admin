import React from 'react'
import AppContent from '../components/AppContent'
import AppSidebar from '../components/AppSidebar'
import Header from '../components/Header'


const DefaultLayout = () => {
  return (
    <div>
      <AppSidebar />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <Header />
        <div className="body flex-grow-1 px-1">
          <AppContent />
        </div>
      </div>
    </div>
  )
}

export default DefaultLayout
